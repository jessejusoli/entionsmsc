<?php
//INICIO
// ATENÇÃO: FAVOR ACOMPANHAR TODOS OS COMENTARIOS E A ESTRUTURA DO CODIGO QUE É BEM SIMPLES!

// Você pode basear-se neste script para enviar um post ou usalo internamente, para gerar o boleto dentro do seu sistema, segue o codigo na integra
/** Ah... Para Usalo dentro do seu sistema,
 * Simplesmente coloque um botão para chamar este script e substitua os valores variaveis, por varias que vao pegar os dados dos usuarios da sessão,
 * ou de um select no banco de dados, 
 Divirta-se!
**/


// Definição do servidor gateway
define('URL_GATEWAY', 'https://goletomoduleshowboleto-jessejusoli.c9.io');//[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI, para o hostname, que irei te passar posteriormente]

// Definição das variaveis, subistituir os exemplos variaveis apenas,
// NUNCA OS FIXOS, que são aqueles que possuiem o comentario no final da linha : Fixo nao mudar
$myarrey = array('issueremail' => 'financeiro@ecossistem.net',// Fixo nao mudar
  	'issuertoken' => '21072269-C3DB-444C-BB79-262BF39BB7DB',// Fixo nao mudar
  	'issuerprofile' => 'Primary',// Fixo nao mudar
    'invoiceid' => '34', //Invoice ID [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'invoicetitle' => 'Recarga de Credito',// Fixo nao mudar
  	'invoicedescription' => 'Recarga de Credito.',// Fixo nao mudar
  	'invoiceamount' => '100.00',//Amount [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
    'invoicedate' => '2015-9-24',//Timestamp or Date and Time. [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'invoicecurrency' => 'BRL',// Fixo nao mudar
  	'invoicecurrencytype' => 'ISO',// Fixo nao mudar
  	'invoicetaxforissuer' => '0',// Fixo nao mudar
  	'invoiceforcenetamount' => '1',// Fixo nao mudar
  	'userfirstname' => 'Roberto',//User's first name [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userlastname' => 'Silva',//User's last name [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'useremail' => 'jessejusoli+34ogo@gmail.com',//User's email [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userdoc' => '9876543212345',//User's Tax id number [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'useraddress1' => 'Logradouro com, 1234',// User's address 1 [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'useraddress2' => 'Bairro',// User's address 2 [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'usercity' => 'Cidade',// [ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userstate' => 'Estado -Provincia', //[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userpostalcode' => '54321-123',//[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'usercountry' => 'Pais', //[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userphone1' => '9876543456', //[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'userphone2' => '8765456767', //[ATENÇÃO ESTE É UM CAMPO VARIAVEL, ALTERAR AQUI]
  	'usersocialprofile' => '', //Perfil de rede Social (faceook/twitter/G+/linkedin/etc) Pode ser: Fixo nao mudar
  	'gatewayv' => '1', // Fixo nao mudar
  	'birthdate' => '1986-11-28');


//Definição final da URL que será chamda
$url = URL_GATEWAY."/payment/index.php";
$uri = $url;
//Condificando para JSON
$contentxxx = json_encode($myarrey);


//Preparando POST
//------------------------------------------------------

$options = array(
  'http' => array(
    'method'  => 'POST',
    'content' => json_encode( $myarrey ),
    'header'=>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
    )
);

//Chamando o POST
$context  = stream_context_create( $options );
$result = file_get_contents( $url, false, $context );
$response = json_decode( $result );


//Apresenta o ResultadoEm Duas opções, são elas: A) Apresenta o Boleto B) Apresenta mensagem de ERRO. 
echo $result;


  
// FIM
?>