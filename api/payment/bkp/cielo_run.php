            <?php
            
            
            $order = new stdClass();
            $order->OrderNumber = $invoiceidx;
            $order->SoftDescriptor = 'MakeMOney';
            $order->Cart = new stdClass();
            $order->Cart->Discount = new stdClass();
            $order->Cart->Discount->Type = 'Percent';
            $order->Cart->Discount->Value = 0;
            $order->Cart->Items = array();
            $order->Cart->Items[0] = new stdClass();
            $order->Cart->Items[0]->Name = $invoicetitlex;
            $order->Cart->Items[0]->Description = $invoicedescriptionx;
            $order->Cart->Items[0]->UnitPrice = ($valor_boleto *100);
            $order->Cart->Items[0]->Quantity = 1;
            $order->Cart->Items[0]->Type = 'Asset';
            $order->Cart->Items[0]->Sku = 'Produto Digital';
            $order->Cart->Items[0]->Weight = 0;
            $order->Shipping = new stdClass();
            $order->Shipping->Type = 'Correios';
            $order->Shipping->SourceZipCode = '01013000';
            
            $order->Customer = new stdClass();
            $order->Customer->Identity = $userdocx;
            $order->Customer->FullName = $userfirstnamex." ".$userlastnamex;
            $order->Customer->Email = $useremailx;
            $order->Customer->Phone = $userphonen1x;
            $order->Options = new stdClass();
            $order->Options->AntifraudEnabled = false;