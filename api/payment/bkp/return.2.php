<?php
$debug = false; 
$issueremailx; $issuertokenx; $issuerprofilex;$invoiceidx; $invoicetitlex; $invoicedescriptionx; $invoiceamountx;
$invoicedatex ; $invoicecurrencyx; $invoicecurrencytypex; $invoicetaxforissuerx; $invoiceforcenetamountx; $userfirstnamex; 
$userlastnamex; $useremailx; $userdocx; $useraddress1x; $useraddress2x; $usercityx; $userstatex; $userpostalcodex; $usercountryx;
$userphonen1x; $userphonen2x; $usersocialprofilex; $invoiceidextra; $issuerprofile_title; $issuerprofile_operator; 
$issuerprofile_amount; $issuerprofile_acceptedcurrency; $issuerprofile_acceptedcurrencytype;

// A sessão precisa ser iniciada em cada página diferente
if (!isset($_SESSION)) session_start();
// Verifica se não há a variável da sessão que identifica o usuário
if (!isset($_SESSION['issueremail'])&& $_SESSION['issuertoken']&&$_SESSION['issuerprofile']) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  //header("Location: http://google.com"); 
  echo "<br/><h1>Aconteceu algum erro com asessao, tente novamente</h1><br/>";
  
  exit;
}else{
        //Check Issuers Profile if ok, return values.

        global $debug, $issueremailx, $issuertokenx, $issuerprofilex, $invoiceidx, $invoicetitlex, $invoicedescriptionx, $invoiceamountx,
        $invoicedatex , $invoicecurrencyx, $invoicecurrencytypex, $invoicetaxforissuerx, $invoiceforcenetamountx, $userfirstnamex, 
        $userlastnamex, $useremailx, $userdocx, $useraddress1x, $useraddress2x, $usercityx, $userstatex, $userpostalcodex, $usercountryx,
        $userphonen1x, $userphonen2x, $usersocialprofilex, $invoiceidextra, $issuerprofile_title, $issuerprofile_operator, 
        $issuerprofile_amount, $issuerprofile_acceptedcurrency, $issuerprofile_acceptedcurrencytype;
        $ipaddress = $_SERVER['REMOTE_ADDR'];
        
        $issueremailx = $_SESSION['issueremail'];
        $issuertokenx = $_SESSION['issuertoken'];
        $issuerprofilex= $_SESSION['issuerprofile'];
        
        //echo "<br/>".$issueremailx;
        //echo "<br/>".$issuertokenx;
        //echo "<br/>".$issuerprofilex;
        
        
        //$debug = true;
        
        require 'config.php';
        // Create connection
        $conn = new mysqli($host, $username, $password, $db_name);
        // Check connection
        if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        } 
        
        // Define $issuerid and $invoiceid and $userid
        $mysqlx_issuerprofilex = $issuerprofilex;
        $mysqlx_issuertokenx = $issuertokenx; 
        $mysqlx_issueremailx = $issueremailx; 

        $sql="SELECT returnurl FROM issuer_profile WHERE issuerid IN (SELECT id FROM issuer WHERE email='$mysqlx_issueremailx' AND token='$mysqlx_issuertokenx') AND title='$mysqlx_issuerprofilex' LIMIT 1";
        $result = $conn->query($sql);
        
        if ($result->num_rows == 1) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
                $issuerprofile_title = $row["returnurl"];
                $GLOBALS['issuerprofile_title'] = $issuerprofile_title;
                //echo $issuerprofile_title;
                if($debug==true){
                    echo "# ----- Check Issuer Profile ----- #<br/>";
                    echo "Rows 1 - With While rows found <br/>";
                    echo "--------------<br/>";
                    echo "issuer Profile: ".$issuerprofilex."<br/>";
                    echo "issuer Profile: ".$mysqlx_issuerprofilex."<br/>";
                    echo "issuer Token: ".$issuertokenx."<br/>";
                    echo "issuer Token: ".$mysqlx_issuertokenx."<br/>";
                    echo "issuer Email: ".$issueremailx."<br/>";
                    echo "issuer Email: ".$mysqlx_issueremailx."<br/>"; 
                    echo "# ----- Check Issuer Profile ----- #<br/>";
                }
                allOk();
        }
        } else {
            if($debug==true){
                echo "# ----- Check Issuer Profile ----- #<br/>";
                echo "Rows 0 - Else if/While Rows found<br/>";
                echo "--------------<br/>";
                echo "issuer Profile: ".$issuerprofilex."<br/>";
                echo "issuer Profile: ".$mysqlx_issuerprofilex."<br/>";
                echo "issuer Token: ".$issuertokenx."<br/>";
                echo "issuer Token: ".$mysqlx_issuertokenx."<br/>";
                echo "issuer Email: ".$issueremailx."<br/>";
                echo "issuer Email: ".$mysqlx_issueremailx."<br/>"; 
                echo "# ----- Check Issuer Profile ----- #<br/>";
            }
            noOk();
        }

}

function allOk(){
    echo "<br/><h1>All OK!</h1><br/>";
    session_destroy();
    header("Location: ".$issuerprofile_title);
    //
}
function noOk(){
    echo "<br/><h1>Dados de Sessao Infalidos, se o erro pesistir contato o suporte!</h1><br/>";
}
?>

