-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Máquina: localhost:3306
-- Data de Criação: 28-Out-2015 às 12:25
-- Versão do servidor: 5.5.46-cll
-- versão do PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `goletoga_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `allowip`
--

CREATE TABLE IF NOT EXISTS `allowip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `srvtitle` varchar(255) DEFAULT NULL,
  `srvip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(23) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) NOT NULL,
  `invoiceid` varchar(255) NOT NULL,
  `profiletitle` varchar(255) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(23,2) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currencytype` varchar(255) DEFAULT NULL,
  `taxforissuer` int(1) NOT NULL DEFAULT '0',
  `forcenetamount` int(1) NOT NULL DEFAULT '0',
  `paid` int(1) NOT NULL DEFAULT '0',
  `paidstatus` varchar(255) DEFAULT NULL,
  `paidbymethod` varchar(255) DEFAULT NULL,
  `canceled` int(1) NOT NULL DEFAULT '0',
  `canceledstatus` varchar(255) DEFAULT NULL,
  `refunded` int(1) NOT NULL DEFAULT '0',
  `refundedstatus` varchar(255) DEFAULT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockedstatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Extraindo dados da tabela `invoice`
--

INSERT INTO `invoice` (`id`, `issuerid`, `invoiceid`, `profiletitle`, `userid`, `title`, `description`, `amount`, `currency`, `currencytype`, `taxforissuer`, `forcenetamount`, `paid`, `paidstatus`, `paidbymethod`, `canceled`, `canceledstatus`, `refunded`, `refundedstatus`, `blocked`, `blockedstatus`) VALUES
(30, 2, '831', 'Primary', 32, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(31, 2, '34', 'Primary', 34, 'Recarga de Credito', 'Recarga de Credito.', '100.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(32, 2, '3434566', 'Primary', 35, 'Recarga de Credito', 'Recarga de Credito.', '1.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(34, 1, '1002', 'Primary', 36, 'Recarga de Credito', 'Recarga de Credito.', '1.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(35, 1, '1003', 'Primary', 37, 'Recarga de Credito', 'Recarga de Credito.', '1.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(36, 1, '9999', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '3700.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(37, 1, '6', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '1700.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(38, 1, '7', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '5000.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(39, 1, '8', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '5000.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(40, 1, '1', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '4000.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(41, 1, '2', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '200.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(42, 1, '1002', 'Primary', 37, 'Recarga de Credito', 'Recarga de Credito.', '1.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL),
(43, 1, '1001', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '0.00', 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer`
--

CREATE TABLE IF NOT EXISTS `issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment, tracking number',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Business name, e.g. Apple inc.',
  `lastname` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `confirmedemail` varchar(255) DEFAULT NULL,
  `confirmedemailsec` varchar(255) DEFAULT NULL,
  `confirmeduser` varchar(255) DEFAULT NULL,
  `confirmedproof` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration issuers' AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `issuer`
--

INSERT INTO `issuer` (`id`, `firstname`, `lastname`, `company`, `position`, `email`, `emailsec`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `token`, `confirmedemail`, `confirmedemailsec`, `confirmeduser`, `confirmedproof`) VALUES
(1, 'Jesse', 'Jusoli', 'Jesse Jusoli Inc.', 'CEO', 'oliveira.jesse@gmail.com', 'mkcampanha@gmail.com', 'thrive', 'vi281186', 'vi281186', '4321', '4321', 'D8F1EB19-D3FD-4356-ADE5-FAD757360500', 'ok', 'ok', 'ok', 'ok'),
(2, 'Jorge', 'Amado', 'ECOSSISTEM', 'CEO', 'financeiro@ecossistem.net', 'mkcampanha@gmail.com', 'ecossistem', 'vi281186', 'vi281186', '4321', '4321', '21072269-C3DB-444C-BB79-262BF39BB7DB', 'ok', 'ok', 'ok', 'ok');

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_address`
--

CREATE TABLE IF NOT EXISTS `issuer_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateprovince` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Address issuers.' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `issuer_address`
--

INSERT INTO `issuer_address` (`id`, `issuerid`, `title`, `address1`, `address2`, `city`, `stateprovince`, `postalcode`, `country`) VALUES
(1, 1, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_profile`
--

CREATE TABLE IF NOT EXISTS `issuer_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `businessname` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `trademark` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `picture` longblob,
  `picturepath` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `websitesupport` varchar(255) DEFAULT NULL,
  `emailsupport` varchar(255) DEFAULT NULL,
  `phonesupport` varchar(255) DEFAULT NULL,
  `payinstructions` varchar(255) DEFAULT NULL COMMENT 'Payment instructions.',
  `descriptionpay` varchar(255) DEFAULT NULL COMMENT 'Description of payment',
  `addressid` int(11) DEFAULT NULL,
  `acceptedcurrency` varchar(255) DEFAULT NULL,
  `acceptedcurrencytype` varchar(255) DEFAULT NULL,
  `acceptedcountry` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `issuer_profile`
--

INSERT INTO `issuer_profile` (`id`, `issuerid`, `title`, `businessname`, `tin`, `trademark`, `slogan`, `picture`, `picturepath`, `website`, `email`, `phone`, `websitesupport`, `emailsupport`, `phonesupport`, `payinstructions`, `descriptionpay`, `addressid`, `acceptedcurrency`, `acceptedcurrencytype`, `acceptedcountry`) VALUES
(1, 1, 'Primary', 'WE thrive Group .org', NULL, 'THRIVE', 'WE thrive Group', NULL, '/1/profile/1/1.png', 'www.wetrivegroup.com', 'info@wetrivegroup.com', NULL, 'wetrivegroup.com', 'support@wetrivegroup.com', NULL, NULL, NULL, 1, 'BRL', 'ISO', 'BRL'),
(3, 1, 'Primary', 'ECOS SISTEM', NULL, 'ECOSSISTEM', 'ECOSSISTEM ', NULL, '/1/profile/1/1.png', 'www.ecossistem.net', 'financeiro@ecossistem.net', NULL, 'ecossistem.net', 'financeiro@ecossistem.net', NULL, NULL, NULL, 1, 'BRL', 'ISO', 'BRL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `createdip` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedip` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `socialprofile` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `confirmed` int(1) NOT NULL DEFAULT '0',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `limited` int(1) NOT NULL DEFAULT '0',
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration users.' AUTO_INCREMENT=39 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `created`, `createdip`, `updated`, `updatedip`, `firstname`, `lastname`, `doc`, `company`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `email`, `emailsec`, `phone1`, `phone2`, `socialprofile`, `address1`, `address2`, `city`, `state`, `postalcode`, `country`, `confirmed`, `blocked`, `limited`, `birthdate`) VALUES
(30, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Enival Costa', 'Silva', '84171812372', NULL, NULL, NULL, NULL, NULL, NULL, 'rettoliveira@hotmail.com.br', NULL, '99988139913', '', '', 'rua edson vidigal, 528 - x', 'joÃ£o viana', 'caxias', 'MaranhÃ£o', '65605170', 'brasil', 1, 0, 0, NULL),
(31, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Luciane Jaqueline', 'Focht', '06963009902', NULL, NULL, NULL, NULL, NULL, NULL, 'deninogueirasantos@gmail.com', NULL, '4933287938', '', '', 'RAO: sao pedro, 2021 - e ', 'jardin america', 'Chapeco', 'SC', '89803402', 'Brasil', 1, 0, 0, NULL),
(32, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Angelica', 'Santos', '36729724870', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanha@gmail.com', NULL, '11948612520', '', '', 'Rua dos Eucaliptos, 271 - ', 'Vila das Malvinas', 'Guarulhos', 'Sao Paulo', '07145500', 'Brasil', 1, 0, 0, NULL),
(33, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Denise', 'Mara', '91632900149', NULL, NULL, NULL, NULL, NULL, NULL, 'denisemara2012@gmail.com', NULL, '6185616546', '', '', 'quadra 48 conjunto k casa 04, 04 - CASA 04', 'Vila SÃ£o JosÃ©', 'BrasÃ­lia', 'DF', '72748000', 'Brasil', 1, 0, 0, NULL),
(34, '0000-00-00 00:00:00', '167.114.209.129', '0000-00-00 00:00:00', '167.114.209.129', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+34ogo@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, '1986-11-28'),
(35, '0000-00-00 00:00:00', '167.114.209.129', '0000-00-00 00:00:00', '167.114.209.129', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+34offgo@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, '1986-11-28'),
(36, '0000-00-00 00:00:00', '104.154.61.73', '0000-00-00 00:00:00', '104.154.61.73', 'Jesse', 'Jusoli', '123465789', NULL, NULL, NULL, NULL, NULL, NULL, 'oliveira.jesse@gmail.com', NULL, '', '', '', 'Rua dos Eucaliptos 271 ', '', 'Guarulhos', 'SÃ£o Paulo', '13245', 'BR', 1, 0, 0, '1986-11-28'),
(37, '0000-00-00 00:00:00', '45.55.203.11', '0000-00-00 00:00:00', '45.55.203.11', 'angelica', 'silva', '09876543210', NULL, NULL, NULL, NULL, NULL, NULL, 'angelica.jesse@gmail.com', NULL, '', '', '', 'Rua dos Eucalipitos 271 ', '', 'Guarulhos', 'SÃ£o Paulo', '12345655', 'BR', 1, 0, 0, '1986-11-28'),
(38, '0000-00-00 00:00:00', '187.64.45.176', '0000-00-00 00:00:00', '187.64.45.176', 'Master', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'juniorjp1989@gmail.com', NULL, '', '', '', 'Av. EpitÃ¡cio Pessoa, Sl 304', '', '', '', '58040000', 'AF', 1, 0, 0, '1986-11-28');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
