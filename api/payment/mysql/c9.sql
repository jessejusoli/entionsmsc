-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 30-Out-2015 às 06:35
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `c9`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `allowip`
--

CREATE TABLE IF NOT EXISTS `allowip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `issuerid` int(11) DEFAULT NULL,
  `srvtitle` varchar(255) DEFAULT NULL,
  `srvip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bankaccount`
--

CREATE TABLE IF NOT EXISTS `bankaccount` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `userid` int(13) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `branch` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `complement` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(23) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) NOT NULL,
  `invoiceid` varchar(255) NOT NULL,
  `profiletitle` varchar(255) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` decimal(23,2) DEFAULT NULL,
  `amountpaid` decimal(23,2) DEFAULT NULL,
  `amountnet` decimal(23,2) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currencytype` varchar(255) DEFAULT NULL,
  `taxforissuer` int(1) NOT NULL DEFAULT '0',
  `forcenetamount` int(1) NOT NULL DEFAULT '0',
  `paid` int(1) NOT NULL DEFAULT '0',
  `paidstatus` varchar(255) DEFAULT NULL,
  `paidbymethod` varchar(255) DEFAULT NULL,
  `canceled` int(1) NOT NULL DEFAULT '0',
  `canceledstatus` varchar(255) DEFAULT NULL,
  `refunded` int(1) NOT NULL DEFAULT '0',
  `refundedstatus` varchar(255) DEFAULT NULL,
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockedstatus` varchar(255) DEFAULT NULL,
  `lastcallback` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Extraindo dados da tabela `invoice`
--

INSERT INTO `invoice` (`id`, `issuerid`, `invoiceid`, `profiletitle`, `userid`, `title`, `description`, `amount`, `amountpaid`, `amountnet`, `currency`, `currencytype`, `taxforissuer`, `forcenetamount`, `paid`, `paidstatus`, `paidbymethod`, `canceled`, `canceledstatus`, `refunded`, `refundedstatus`, `blocked`, `blockedstatus`, `lastcallback`) VALUES
(30, 2, '831', 'Primary', 32, 'Recarga de CrÃ©dito', 'Recarga de CrÃ©dito.', '100.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(31, 2, '34', 'Primary', 34, 'Recarga de Credito', 'Recarga de Credito.', '100.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(32, 2, '3434566', 'Primary', 35, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(34, 1, '1002', 'Primary', 36, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(35, 1, '1003', 'Primary', 37, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(36, 1, '9999', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '3700.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(37, 1, '6', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '1700.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(38, 1, '7', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '5000.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(39, 1, '8', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '5000.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(40, 1, '1', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '4000.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(41, 1, '2', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '200.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(42, 1, '1002', 'Primary', 37, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(43, 1, '1001', 'Primary', 38, 'Recarga de Credito', 'Recarga de Credito.', '0.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(44, 1, '1047', 'Primary', 39, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '60.00', NULL, NULL, 'USD', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(45, 1, '1087', 'Primary', 40, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(46, 1, '1002', 'Primary', 41, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(47, 1, '1047', 'Primary', 0, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(48, 1, '104347', 'Primary', 42, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(49, 1, '4347', 'Primary', 43, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(50, 1, '47', 'Primary', 44, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(51, 1, '47', 'Primary', 45, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(52, 1, '47', 'Primary', 46, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(53, 1, '7', 'Primary', 47, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(54, 1, '8', 'Primary', 47, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(55, 1, '9', 'Primary', 47, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(56, 1, '10', 'Primary', 47, 'THRIVE-CHARGE-1001-90', 'Credit recharge.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(57, 1, '1003', 'Primary', 48, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(58, 1, '1028', 'Primary', 49, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(59, 1, '1029', 'Primary', 50, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(60, 1, '1028', 'Primary', 50, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(61, 1, '1030', 'Primary', 51, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(62, 1, '1031', 'Primary', 52, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(63, 1, '1032', 'Primary', 53, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(64, 1, '1033', 'Primary', 54, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(65, 1, '1034', 'Primary', 55, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(66, 1, '1035', 'Primary', 56, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(67, 1, '1036', 'Primary', 57, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(68, 1, '1037', 'Primary', 58, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL),
(69, 1, '1038', 'Primary', 59, 'Recarga de Credito', 'Recarga de Credito.', '1.00', NULL, NULL, 'BRL', 'ISO', 0, 1, 0, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice_token`
--

CREATE TABLE IF NOT EXISTS `invoice_token` (
  `id` int(13) NOT NULL AUTO_INCREMENT,
  `issuerid` int(13) DEFAULT NULL,
  `issuerprofile` varchar(255) DEFAULT NULL,
  `userid` int(13) DEFAULT NULL,
  `payurl` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Extraindo dados da tabela `invoice_token`
--

INSERT INTO `invoice_token` (`id`, `issuerid`, `issuerprofile`, `userid`, `payurl`, `token`, `datecreated`) VALUES
(13, 1, 'Primary', 47, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzRlMTFmMDM1LTdlZjUtNDY4NS05Mzk5LWJkZTNmYzI2Y2UwOA==', 'ee4d9da4-c0f2-4d1d-89e4-1a19fef42d59', '2015-10-30 00:03:27'),
(14, 1, 'Primary', 40, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzRhMGQxNmQ0LTBkYjAtNDY3NS1hYzYxLTc0ZGE3ZjUxMTFlOA==', 'e92a03b3-98cd-4b44-81b7-112b00b0d36f', '2015-10-30 00:04:36'),
(15, 1, 'Primary', 40, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2VkOTU5YTU2LTQ0MTMtNDFmZS1iNTYxLWRhZWQ0ZTQwM2UxOA==', '77fc1041-8cf7-4db8-9a62-41558872bf0f', '2015-10-30 00:07:28'),
(16, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2I3MzI1MGNkLTBmYWEtNGE4NC05ZWFmLTIwMGM3Nzc5YTM5YQ==', 'dc590290-5937-4042-8064-8d678b0ddeb5', '2015-10-30 00:09:40'),
(17, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2JkZjE2NWQzLTU5YmUtNDUzYS1hYzE2LWMzMmUzOGIwYmFiYg==', '71c48256-2138-4a10-9c2b-409da17e6a43', '2015-10-30 00:10:53'),
(18, 1, 'Primary', 47, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzRjMzg5YThkLTljNmMtNGEzNi04ODgwLTlhNjA2N2YyZjY1MA==', 'afacc777-b03d-41ad-83bf-805227cf8891', '2015-10-30 00:16:48'),
(19, 1, 'Primary', 40, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2U2MjhmOTg4LTAzNTktNDY1OC1iZWU0LTRhNzIxODkwNGFjOA==', 'aac16931-a632-4df3-aab1-19c0b9e4b456', '2015-10-30 00:17:35'),
(20, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2QzMjQwZjdlLTcyM2YtNDY4Ny05YmY5LWJkOGUyMGViMWRkYg==', '09813c71-2f92-435f-ae79-1e0289fb82ec', '2015-10-30 00:17:49'),
(21, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2ViZjNiOWMxLTk3MDEtNGY5OC04NmQ4LTVlMmMzNjIxODNjMA==', 'e6925448-565f-478a-b3e1-749128eae67c', '2015-10-30 00:17:52'),
(22, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzQ4M2YyMTIwLTQwYWYtNDRhNS1hYTg5LTRhNWQyMWQ2MTUwYw==', '1ade7632-1b98-42b1-a16f-935938cc0bcf', '2015-10-30 00:18:45'),
(23, 1, 'Primary', 47, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzA1NjgzZGE2LTU0OTctNDdhMC1iNjhkLWEzNzcyY2U1MTA5YQ==', 'bbcd86c1-c7ae-4a3e-bc57-83b75569b828', '2015-10-30 00:37:05'),
(24, 1, 'Primary', 49, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzBmZDA3OWRiLTBkMWEtNGY2YS1iMTBlLTdiMDg3ZmNhZGZiNg==', '2309d604-4119-4bec-86c3-b2327a6d553c', '2015-10-30 00:39:37'),
(25, 1, 'Primary', 49, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzU0ZmY4NWE5LTBjYjAtNDcyMi05YmI3LTU1ZmUwYjQ1NGYwNw==', '9e3c541c-14cf-4ae9-af45-b667be828a9d', '2015-10-30 00:40:38'),
(26, 1, 'Primary', 49, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzA3ZDY0ZGY4LWM3OWYtNDY3My1hZjdiLTMyZTgyOTEwNTJhZg==', '41540524-59d6-46af-b55c-de40156ab79d', '2015-10-30 00:41:30'),
(27, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2VhNWI3MmU3LWM4MWUtNDY3YS04YmU4LWFjNGNhMGUwZmE2MA==', 'd9f2837a-61b8-4920-9a2f-ca14762948d9', '2015-10-30 00:43:09'),
(28, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2IwMWM0NDM3LWVhZWYtNDk3YS1hMGNiLWMzNDQ5ZTk3MjNiNA==', 'b882773f-46e6-42d0-b9ff-b03dd57c5f0e', '2015-10-30 00:51:18'),
(29, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2E3ZjFhNWI4LTZjMWYtNDQzZS05ODMwLWUzYTBmMGJkMmRkYw==', '54d18fb1-5faf-4308-bb01-15d8b2215554', '2015-10-30 01:02:53'),
(30, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzA4M2Y0ZGIzLTZkNmMtNDUzNi04MjM2LTdhMDMyMGMwMmM0NA==', 'bc431d48-d736-46f7-885a-ef65d595e4de', '2015-10-30 01:46:23'),
(31, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4Lzc2NDNjNGNkLTQzNjUtNGJlZi1iOGI1LTkxNzBhYzEyY2VkZQ==', '9f379f7e-1218-46a6-ae15-e293c65edc67', '2015-10-30 01:49:45'),
(32, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2JmZTMwZmY1LTdlYjMtNDA5Ni04ZjZkLTUxNzJhYmQwYmJmNQ==', '469769ba-83cb-4a0e-89be-780e0e87053c', '2015-10-30 01:52:48'),
(33, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzVkZmYzZTNhLTNlNjYtNDA1My04ZmJjLWQyMTFjYThiYzFjMQ==', '06402019-1428-42cb-a033-ec4f17fc2736', '2015-10-30 01:54:08'),
(34, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzUwMGNkZmYxLWYzMWMtNGMzMC1hODY4LTkwZDQ5ZDc3MDIwZg==', 'a21b7767-f563-43cf-9e04-a88fa428ec22', '2015-10-30 01:55:01'),
(35, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzhhNjczNTY1LTYwYzQtNGIwOC04MzQzLWRlZDQxNTc1NDVkMA==', '8be9aa91-4798-4f2e-9a15-76241bea4937', '2015-10-30 01:56:29'),
(36, 1, 'Primary', 49, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzU0ZWEyYTE3LWFmZWEtNDFkMi1iNDkzLTY0OTY4N2U2NmZiZA==', '53604557-aa3c-42fc-8f38-d39bbd098444', '2015-10-30 01:59:25'),
(37, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2EzOTU1MTcyLTM4ZWQtNGQ3Ni05ZjQzLWNkYWM0NzU5YTViZQ==', 'fbd916de-9496-458b-8417-30d065caf225', '2015-10-30 02:03:51'),
(38, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2Q1ZWNiMzE1LTMwZWYtNDQ4OS1hM2NkLTU1MmVlMzExNDFmNA==', 'b9624a57-76dd-4a5b-aec9-1cdba2100817', '2015-10-30 02:12:32'),
(39, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzAyYzljNmRlLTY3YWYtNGZmYS04MWZhLWI1YTI3NzAxNDcxNA==', '92112aff-38de-4ff8-ad10-40fef69a6951', '2015-10-30 02:22:49'),
(40, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzU4ZDk1NGFkLTI4MWQtNDE4Mi1hZjM1LTM1ZWU2NzNkNjFjNg==', '18d4fa10-26f7-4c8e-94e4-1a85d8350a0a', '2015-10-30 02:26:03'),
(41, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzczOWIzN2NiLWE3MjAtNDllYy1iOTk0LTkxYWMyNWVhNTRmOQ==', '9bcb61f3-4614-457f-a34e-3d4f55e66fc1', '2015-10-30 02:28:03'),
(42, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzljODBhOTkyLWQyNDItNDNlMy04Y2NlLTgwNDNhYjRjNjY1MA==', '38756187-dfab-4270-aad9-48dad3250a60', '2015-10-30 02:29:33'),
(43, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2Y5MWJiMDA3LWE3N2ItNDIyNC05YTM0LTZkYmVkMWI3ZDJmMg==', 'c894376f-4097-4273-8a90-587c692b2742', '2015-10-30 02:30:56'),
(44, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzU5ZGQ3NGMxLWUwZjItNDljYy04YzYwLTI2MTIwMWU0ZDM5Mg==', '9349fbc6-cf16-4ecb-87e8-97e715396c76', '2015-10-30 02:42:22'),
(45, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzJhM2U3ODNhLTI3N2EtNDAyNy1hODAzLWNmMGM3OWYyYTU5OQ==', '449453ba-52b1-4903-a396-a03a38536e96', '2015-10-30 02:45:29'),
(46, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2NhYjU5ZTYxLWRiOGQtNGIzNy1hMjAyLWMzYjUwMGQ2OTgyNA==', 'ccdb4ed1-dcba-4463-bcac-2b7a947a50e5', '2015-10-30 02:45:31'),
(47, 1, 'Primary', 50, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzA1NjA4YWUyLWZlOGQtNDQ5Mi1iZTUyLWJmZTU3MmQ2ZGNkNg==', '50fb7445-779e-4bec-b26f-d38e19f3b54f', '2015-10-30 02:45:33'),
(48, 1, 'Primary', 48, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzA4ZTJjYmU4LTAxMjMtNDJiOS04NDg1LTcwOThiZTI4ZTlhZA==', 'ad649d17-d409-43a7-9c21-02520d892f68', '2015-10-30 02:50:05'),
(49, 1, 'Primary', 47, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2I5ZGNlOGE5LTcwNzUtNGY5Ny05ZmI0LTMyNzgzMmEzYzE2Zg==', '4214b0d7-6a65-4ada-8f37-9cbf3b83b784', '2015-10-30 05:16:41'),
(50, 1, 'Primary', 51, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2M2NzQxZDM5LWE4MDUtNGE2NS1iZThlLWM4Njg3NDY3ZDkwMQ==', 'cd2f0bc5-7909-4600-8f22-0cf7bfee66e3', '2015-10-30 05:26:50'),
(51, 1, 'Primary', 52, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzhjM2Y0YzVmLTQ2YjgtNDk4Ny1hYjZiLTMyZDczMzA1ZWViNg==', '653220c6-252f-43de-92de-f40c5a68ced3', '2015-10-30 06:02:01'),
(52, 1, 'Primary', 52, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzI1YjI5NDQ5LWUyMTEtNDMwNi1iZjU1LWUyN2VmMzg4MGU0OA==', 'b67c261d-0d37-49b3-8bdb-58ad14d05dcc', '2015-10-30 06:02:37'),
(53, 1, 'Primary', 47, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2Q4NDg4NzA2LTk3Y2YtNDRkOS1hYmQyLTY2ZGMxMzNjMmRlNA==', '6fc71e0f-3553-4584-bdb2-22466d68992a', '2015-10-30 06:03:01'),
(54, 1, 'Primary', 53, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2VhZDhlNGI4LWM0OGItNGIxMi1hMzk0LWE5OTBlMmVkZDVlYg==', '6ed64839-4324-4a26-b564-289295643471', '2015-10-30 06:08:46'),
(55, 1, 'Primary', 54, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4LzkxNjZiNzA1LWY3MGItNDJhZS1iNWNlLTY1Yzg2YTNiNDUwYw==', 'f24a7e56-2be5-4839-8da4-8ee025e8dd68', '2015-10-30 06:11:41'),
(56, 1, 'Primary', 55, 'aHR0cHM6Ly9jaWVsb2Vjb21tZXJjZS5jaWVsby5jb20uYnIvVHJhbnNhY3Rpb25hbFZOZXh0L0NoZWNrb3V0L0luZGV4L2MyODU4ZGI0LTUyYzMtNDM2My1iYzVjLTgzZGEwOWU2MmNiNQ==', 'ea50e70a-ed58-4635-b1e6-b0af977ccea5', '2015-10-30 06:15:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer`
--

CREATE TABLE IF NOT EXISTS `issuer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto Increment, tracking number',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Business name, e.g. Apple inc.',
  `lastname` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `confirmedemail` varchar(255) DEFAULT NULL,
  `confirmedemailsec` varchar(255) DEFAULT NULL,
  `confirmeduser` varchar(255) DEFAULT NULL,
  `confirmedproof` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration issuers' AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `issuer`
--

INSERT INTO `issuer` (`id`, `firstname`, `lastname`, `company`, `position`, `email`, `emailsec`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `token`, `confirmedemail`, `confirmedemailsec`, `confirmeduser`, `confirmedproof`) VALUES
(1, 'Jesse', 'Jusoli', 'Jesse Jusoli Inc.', 'CEO', 'oliveira.jesse@gmail.com', 'mkcampanha@gmail.com', 'thrive', 'vi281186', 'vi281186', '4321', '4321', 'D8F1EB19-D3FD-4356-ADE5-FAD757360500', 'ok', 'ok', 'ok', 'ok'),
(2, 'Jorge', 'Amado', 'ECOSSISTEM', 'CEO', 'financeiro@ecossistem.net', 'mkcampanha@gmail.com', 'ecossistem', 'vi281186', 'vi281186', '4321', '4321', '21072269-C3DB-444C-BB79-262BF39BB7DB', 'ok', 'ok', 'ok', 'ok');

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_address`
--

CREATE TABLE IF NOT EXISTS `issuer_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `stateprovince` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Address issuers.' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `issuer_address`
--

INSERT INTO `issuer_address` (`id`, `issuerid`, `title`, `address1`, `address2`, `city`, `stateprovince`, `postalcode`, `country`) VALUES
(1, 1, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `issuer_profile`
--

CREATE TABLE IF NOT EXISTS `issuer_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issuerid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `businessname` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `trademark` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `picture` longblob,
  `picturepath` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `websitesupport` varchar(255) DEFAULT NULL,
  `emailsupport` varchar(255) DEFAULT NULL,
  `phonesupport` varchar(255) DEFAULT NULL,
  `payinstructions` varchar(255) DEFAULT NULL COMMENT 'Payment instructions.',
  `descriptionpay` varchar(255) DEFAULT NULL COMMENT 'Description of payment',
  `returnurl` text,
  `callbackurl` text,
  `addressid` int(11) DEFAULT NULL,
  `acceptedcurrency` varchar(255) DEFAULT NULL,
  `acceptedcurrencytype` varchar(255) DEFAULT NULL,
  `acceptedcountry` varchar(255) DEFAULT NULL,
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `issuer_profile`
--

INSERT INTO `issuer_profile` (`id`, `issuerid`, `title`, `businessname`, `tin`, `trademark`, `slogan`, `picture`, `picturepath`, `website`, `email`, `phone`, `websitesupport`, `emailsupport`, `phonesupport`, `payinstructions`, `descriptionpay`, `returnurl`, `callbackurl`, `addressid`, `acceptedcurrency`, `acceptedcurrencytype`, `acceptedcountry`, `token`) VALUES
(1, 1, 'Primary', 'WE thrive Group .org', NULL, 'THRIVE', 'WE thrive Group', NULL, '/1/profile/1/1.png', 'www.wetrivegroup.com', 'oliveira.jesse@gmail.com', NULL, 'wetrivegroup.com', 'support@wetrivegroup.com', NULL, NULL, NULL, 'http://whatsappmakemoney.ga/financeiro/faturas', 'http://whatsappmakemoney.ga/financeiro/payment_callback', 1, 'BRL', 'ISO', 'BRL', 'D8F1EB19-D3FD-4356-ADE5-FAD757360500'),
(3, 1, 'Primary', 'ECOS SISTEM', NULL, 'ECOSSISTEM', 'ECOSSISTEM ', NULL, '/1/profile/1/1.png', 'www.ecossistem.net', 'financeiro@ecossistem.net', NULL, 'ecossistem.net', 'financeiro@ecossistem.net', NULL, NULL, NULL, NULL, NULL, 1, 'BRL', 'ISO', 'BRL', '21072269-C3DB-444C-BB79-262BF39BB7DB');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `createdip` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedip` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `doc` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordhash` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  `pinhash` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `emailsec` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `phone2` varchar(255) DEFAULT NULL,
  `socialprofile` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `confirmed` int(1) NOT NULL DEFAULT '0',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `limited` int(1) NOT NULL DEFAULT '0',
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Registration users.' AUTO_INCREMENT=60 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `created`, `createdip`, `updated`, `updatedip`, `firstname`, `lastname`, `doc`, `company`, `username`, `password`, `passwordhash`, `pin`, `pinhash`, `email`, `emailsec`, `phone1`, `phone2`, `socialprofile`, `address1`, `address2`, `city`, `state`, `postalcode`, `country`, `confirmed`, `blocked`, `limited`, `birthdate`) VALUES
(30, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Enival Costa', 'Silva', '84171812372', NULL, NULL, NULL, NULL, NULL, NULL, 'rettoliveira@hotmail.com.br', NULL, '99988139913', '', '', 'rua edson vidigal, 528 - x', 'joÃ£o viana', 'caxias', 'MaranhÃ£o', '65605170', 'brasil', 1, 0, 0, NULL),
(31, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Luciane Jaqueline', 'Focht', '06963009902', NULL, NULL, NULL, NULL, NULL, NULL, 'deninogueirasantos@gmail.com', NULL, '4933287938', '', '', 'RAO: sao pedro, 2021 - e ', 'jardin america', 'Chapeco', 'SC', '89803402', 'Brasil', 1, 0, 0, NULL),
(32, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Angelica', 'Santos', '36729724870', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanha@gmail.com', NULL, '11948612520', '', '', 'Rua dos Eucaliptos, 271 - ', 'Vila das Malvinas', 'Guarulhos', 'Sao Paulo', '07145500', 'Brasil', 1, 0, 0, NULL),
(33, '0000-00-00 00:00:00', '188.166.13.147', '0000-00-00 00:00:00', '188.166.13.147', 'Denise', 'Mara', '91632900149', NULL, NULL, NULL, NULL, NULL, NULL, 'denisemara2012@gmail.com', NULL, '6185616546', '', '', 'quadra 48 conjunto k casa 04, 04 - CASA 04', 'Vila SÃ£o JosÃ©', 'BrasÃ­lia', 'DF', '72748000', 'Brasil', 1, 0, 0, NULL),
(34, '0000-00-00 00:00:00', '167.114.209.129', '0000-00-00 00:00:00', '167.114.209.129', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+34ogo@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, '1986-11-28'),
(35, '0000-00-00 00:00:00', '167.114.209.129', '0000-00-00 00:00:00', '167.114.209.129', 'Roberto', 'Silva', '9876543212345', NULL, NULL, NULL, NULL, NULL, NULL, 'jessejusoli+34offgo@gmail.com', NULL, '9876543456', '8765456767', '', 'Logradouro com, 1234', 'Bairro', 'Cidade', 'Estado -Provincia', '54321-123', 'Pais', 1, 0, 0, '1986-11-28'),
(36, '0000-00-00 00:00:00', '104.154.61.73', '0000-00-00 00:00:00', '104.154.61.73', 'Jesse', 'Jusoli', '123465789', NULL, NULL, NULL, NULL, NULL, NULL, 'oliveira.jesse@gmail.com', NULL, '', '', '', 'Rua dos Eucaliptos 271 ', '', 'Guarulhos', 'SÃ£o Paulo', '13245', 'BR', 1, 0, 0, '1986-11-28'),
(37, '0000-00-00 00:00:00', '45.55.203.11', '0000-00-00 00:00:00', '45.55.203.11', 'angelica', 'silva', '09876543210', NULL, NULL, NULL, NULL, NULL, NULL, 'angelica.jesse@gmail.com', NULL, '', '', '', 'Rua dos Eucalipitos 271 ', '', 'Guarulhos', 'SÃ£o Paulo', '12345655', 'BR', 1, 0, 0, '1986-11-28'),
(38, '0000-00-00 00:00:00', '187.64.45.176', '0000-00-00 00:00:00', '187.64.45.176', 'Master', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'juniorjp1989@gmail.com', NULL, '', '', '', 'Av. EpitÃ¡cio Pessoa, Sl 304', '', '', '', '58040000', 'AF', 1, 0, 0, '1986-11-28'),
(39, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'JessÃ©', 'JoÃ£o', '8123456789340', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampanhaCelisssa@gmail.com', NULL, '+5524976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(40, '0000-00-00 00:00:00', '10.240.0.48', '0000-00-00 00:00:00', '10.240.0.48', 'JessÃ©', 'JoÃ£o', '236729724870', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampsssa@gmail.com', NULL, '11976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(41, '0000-00-00 00:00:00', '10.240.0.61', '0000-00-00 00:00:00', '10.240.0.61', 'Jesse', 'Jusoli', '12345678970', NULL, NULL, NULL, NULL, NULL, NULL, 'oli@gmail.com', NULL, '', '', '', 'Rua 123 compl', '', 'fghj', 'fvcghjk', '132456', 'De', 1, 0, 0, '1986-11-28'),
(42, '0000-00-00 00:00:00', '10.240.0.48', '0000-00-00 00:00:00', '10.240.0.48', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcampansdfhaCelisssa@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(43, '0000-00-00 00:00:00', '10.240.0.28', '0000-00-00 00:00:00', '10.240.0.28', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcamsssa@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(44, '0000-00-00 00:00:00', '10.240.0.40', '0000-00-00 00:00:00', '10.240.0.40', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcamvbsssa@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(45, '0000-00-00 00:00:00', '10.240.0.48', '0000-00-00 00:00:00', '10.240.0.48', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcssa@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(46, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkc@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(47, '0000-00-00 00:00:00', '10.240.0.38', '0000-00-00 00:00:00', '10.240.0.38', 'JessÃ©', 'JoÃ£o', '36724829770', NULL, NULL, NULL, NULL, NULL, NULL, 'mkcc@gmail.com', NULL, '24976545678', '0', 'fb.com/liliandoliveira', 'Rua Porto Rico, 134', 'Vila Americana', 'Volta Redonda', 'SÃ£o Paulo', '212070-230', 'Brasil', 1, 0, 0, '0000-00-00'),
(48, '0000-00-00 00:00:00', '10.240.0.28', '0000-00-00 00:00:00', '10.240.0.28', 'yugi2', '', '4364', NULL, NULL, NULL, NULL, NULL, NULL, 'juniorjp19864436436sgdgdshg9@gmail.com', NULL, '', '', '', 'grgrg dhsdf gdsghdsg', '', 'bxzbx', 'vzxvxbzbx', 'bxcbcx', 'Fi', 1, 0, 0, '1986-11-28'),
(49, '0000-00-00 00:00:00', '10.240.0.41', '0000-00-00 00:00:00', '10.240.0.41', 'dakota', 'johnson', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'asaewdwsssgggg@cccbddfbbsdsb.com', NULL, '', '', '', 'Rua dos Eucalipitos 271 ', '', 'dfghj', 'sdfgh', '12345655', 'Fi', 1, 0, 0, '1986-11-28'),
(50, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'Jesse', 'Jusoli', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olccci@gmail.com', NULL, '', '', '', 'Rua 2123 ', '', 'fdf', 'df', '1223415', 'BT', 1, 0, 0, '1986-11-28'),
(51, '0000-00-00 00:00:00', '10.240.0.48', '0000-00-00 00:00:00', '10.240.0.48', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngdddsg@gm.com', NULL, '1111111111', '', '', 'Rua 123 ', '', 'dsfgsdf', 'sdfg', '2415', 'BT', 1, 0, 0, '1986-11-28'),
(52, '0000-00-00 00:00:00', '10.240.0.41', '0000-00-00 00:00:00', '10.240.0.41', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngdddcdcdcsg@gm.com', NULL, '1111111111', '', '', 'Rua 123 ', '', 'cdscds', 'cdscds', '2415', 'BJ', 1, 0, 0, '1986-11-28'),
(53, '0000-00-00 00:00:00', '10.240.0.61', '0000-00-00 00:00:00', '10.240.0.61', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngdddgabrsg@gm.com', NULL, '1111111111', '', '', 'Rua 123 ', '', 'sdd', 'sfds', '2415', 'BY', 1, 0, 0, '1986-11-28'),
(54, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngdddgadfbrsg@gm.com', NULL, '1111111111', '', '', 'Rua 123 ', '', 'dfdg', 'dsfd', '2415', 'DZ', 1, 0, 0, '1986-11-28'),
(55, '0000-00-00 00:00:00', '10.240.0.42', '0000-00-00 00:00:00', '10.240.0.42', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngssdfghujikolkjg@gffggdfghjklfdghjkm.com', NULL, '1111111111', '', '', 'Rua 123 ', '', 'cdscds', 'cdssdc', '2415', 'BJ', 1, 0, 0, '1986-11-28'),
(56, '0000-00-00 00:00:00', '10.240.0.48', '0000-00-00 00:00:00', '10.240.0.48', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngssdfghujikolkjg@gffggdfghjklfdghjkm.cosdsd', NULL, '1111111111', '', '', 'Rua 123 ', '', 'sdsd', 'dsds', '2415', 'BZ', 1, 0, 0, '1986-11-28'),
(57, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngssdfghujikolkjg@gffggdfghjklfdghjkm.cosddfvdfsd', NULL, '1111111111', '', '', 'Rua 123 ', '', 'dsfdsf', 'fdsf', '2415', 'BJ', 1, 0, 0, '1986-11-28'),
(58, '0000-00-00 00:00:00', '10.240.0.64', '0000-00-00 00:00:00', '10.240.0.64', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngssdfghujikolkjg@gffggdfghjklfdghjkm.cosddfvdfdfsdsdsd', NULL, '1111111111', '', '', 'Rua 123 ', '', 'dsds', 'dsds', '2415', 'De', 1, 0, 0, '1986-11-28'),
(59, '0000-00-00 00:00:00', '10.240.0.49', '0000-00-00 00:00:00', '10.240.0.49', 'angelica', 'silva', '1111111111111', NULL, NULL, NULL, NULL, NULL, NULL, 'olngssdfghujikolkjg@gffggdfghjklfdghjkm.cosddfvdfdfsdscdcddsd', NULL, '1111111111', '', '', 'Rua 123 ', '', 'cdscds', 'cdscds', '2415', 'BZ', 1, 0, 0, '1986-11-28');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
